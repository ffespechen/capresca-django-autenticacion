from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Personal(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="personal", default="avatar5.png")
    biografia = models.TextField(default="Sin biografía")
