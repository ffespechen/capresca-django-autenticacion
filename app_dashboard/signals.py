from django.contrib.auth.models import User 
from django.db.models.signals import post_save 
from django.dispatch import receiver 
from django.contrib.auth.models import Permission, Group
from .models import Personal

@receiver(post_save, sender=User)
def crear_personal(sender, instance, created, **kwargs):

    if created:
        Personal.objects.create(user=instance)

        permiso_lectura_clases = Permission.objects.get(codename='view_modelopruebaparaclases')
        permiso_lectura_funciones = Permission.objects.get(codename='view_modelopruebaparafunciones')

        grupo = Group.objects.get(name='revisores')

        instance.user_permissions.add(permiso_lectura_clases, permiso_lectura_funciones)
        instance.groups.set([grupo, ])

        #-- instance.user_permissions.remove('permiso_lectura_clases')
        #-- instance.user_permissions.set('un_solo_permiso')
        #-- instance.user_permissions.clear()

        # myuser.groups.set([group_list])
        # myuser.groups.add(group, group, ...)
        # myuser.groups.remove(group, group, ...)
        # myuser.groups.clear()

