from django.shortcuts import render 
from django.views.generic import ( TemplateView, 
                                  ListView )

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import ModeloPruebaParaClases

# Create your views here.

class SinAutenticarView(TemplateView):
    template_name = 'sin_autenticacion.html'


class ConAutenticacionView(LoginRequiredMixin, TemplateView):
    template_name = 'con_autenticacion.html'


# class ModeloMixin:
#     model = ModeloPruebaParaClases 

#     def guardar_log(self):
#         print('Hola mundo')


class ModelosListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'lista.html'
    model = ModeloPruebaParaClases
    permission_required = ['app_clases.view_modelopruebaparaclases',]


# class ModelosCreateView(PermissionRequiredMixin, ModeloMixin, CreateView):
#     template_name = ''

#     permission_required = ['app_clases.add_modelopruebaparaclases', ]

# class ModelosUpdateView(PermissionRequiredMixin, UpdateView):
#     template_name = ''
#     model = ModeloPruebaParaClases
#     permission_required = ['app_clases.change_modelopruebaparaclases', ]

# class ModeloDeleteView(PermissionRequiredMixin, DeleteView):
#     template_name = ''
#     model = ModeloPruebaParaClases
#     permission_required = ['app_clases.delete_modelopruebaparaclase', ]