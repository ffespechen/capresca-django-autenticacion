from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from app_clases.models import ModeloPruebaParaClases
from app_clases.api.serializers import ModeloClasesSerializer

class ModeloClasesListView(generics.ListAPIView):
    queryset = ModeloPruebaParaClases.objects.all()
    serializer_class = ModeloClasesSerializer


class ModeloClasesDetailView(generics.RetrieveAPIView):
    queryset = ModeloPruebaParaClases.objects.all()
    serializer_class = ModeloClasesSerializer


class ClasePersonalizadaView(APIView):
    authentication_classes = [BasicAuthentication, ]
    permission_classes = [IsAuthenticated]

    def get(self, request, slug, format=None):
        return Response({ 'vista': 'APIView',
                         'valor_recibido': slug ,
                         'http_metodo': request.method,
                          'usuario': request.user.username })