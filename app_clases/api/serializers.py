from rest_framework import serializers
from app_clases.models import ModeloPruebaParaClases

class ModeloClasesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModeloPruebaParaClases
        fields = ['id', 'nombre', 'descripcion']