from django.urls import path
from  . import views

app_name = 'app_clases'

urlpatterns = [
    path('modelos_clases/', views.ModeloClasesListView.as_view(), name='api_clases_listview'),
    path('modelos_clases/<pk>', views.ModeloClasesDetailView.as_view(), name='app_clases_detailview'),
    path('modelos_clases/<slug>/personalizada/', views.ClasePersonalizadaView.as_view(), name='personalizada'),
]
