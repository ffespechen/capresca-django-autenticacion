from django.apps import AppConfig


class AppClasesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "app_clases"
