from django.urls import path
from .views import UsuarioListView, generar_reporte_csv, generar_reporte_xlsx

app_name = 'usuarios'

urlpatterns = [
    path('listar/', UsuarioListView.as_view(), name='listar'),
    path('reporte-csv/', generar_reporte_csv, name='reporte_csv'),
    path('reporte-xlsx/', generar_reporte_xlsx, name='reporte_xlsx'),
]

