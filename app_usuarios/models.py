from django.db import models

# Create your models here.
class Usuario(models.Model):
    genero = models.CharField(max_length=10)
    titulo = models.CharField(max_length=5)
    apellido = models.CharField(max_length=30)
    nombre = models.CharField(max_length=30)
    ciudad = models.CharField(max_length=50)
    pais = models.CharField(max_length=30)
    codigo_postal = models.PositiveIntegerField()
    email = models.EmailField()
    username = models.CharField(max_length=30)
    telefono = models.CharField(max_length=30)

    def __str__(self):
        return F"{self.apellido.upper()}, {self.nombre.title()} - {self.pais} | {self.codigo_postal}"
    
    class Meta:
        ordering = ['apellido', 'nombre', 'pais', 'ciudad']