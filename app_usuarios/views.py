import csv

from django.http import HttpResponse
from django.shortcuts import render 
from django.views.generic import ListView

from openpyxl import Workbook

from .models import Usuario

# Create your views here.
class UsuarioListView(ListView):
    queryset = Usuario.objects.all()
    template_name = 'usuarios/lista.html'
    paginate_by = 8


def generar_reporte_csv(request):
    """
    Vista basada en funciones que genera un archivo CSV con el padrón de todos los usuarios registrados.
    Sigue el ejemplo propuesto en la documentación oficial de Django
    https://docs.djangoproject.com/en/4.2/howto/outputting-csv/
    """
    usuarios = Usuario.objects.all()

    response = HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="reporte.csv"'
        }
    )

    titulos = ['id', 'titulo', 'apellido', 'nombre', 'genero', 'ciudad', 'pais', 'codigo_postal', 
               'email','telefono', 'username']
    
    # Se construyen las líneas (registros) del archivo CSV a partir de la query realizada
    registros = []
    for usuario in usuarios:
        registros.append([usuario.id, 
                          usuario.titulo,
                          usuario.apellido,
                          usuario.nombre,
                          usuario.genero,
                          usuario.ciudad, 
                          usuario.pais,
                          usuario.codigo_postal,
                          usuario.email,
                          usuario.telefono,
                          usuario.username])

    # Operaciones con el writer
    writer = csv.writer(response, delimiter=';')
    writer.writerow(titulos)
    writer.writerows(registros)

    return response

    
def generar_reporte_xlsx(request):
    """
    Vista basada en funciones para generar un reporte en .xlsx del padrón de usuarios
    Emplea la librería openpyxl 
    https://openpyxl.readthedocs.io/en/stable/
    """

    usuarios = Usuario.objects.all()

    response = HttpResponse(
        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        headers={
            "Content-Disposition": 'attachment; filename="reporte.xlsx"'
        }        
    )

    titulos = ['id', 'titulo', 'apellido', 'nombre', 'genero', 'ciudad', 'pais', 'codigo_postal', 
               'email','telefono', 'username']
    
    wb = Workbook()
    wb.remove_sheet(wb.active)

    ws = wb.create_sheet("Reporte xlsx")

    # Títulos de la hoja de cálculo
    # NOta: por compatibilidad, openpyxl cuenta columnas y filas comenzando desde 1
    columna = 1
    fila = 1
    for titulo in titulos:
        ws.cell(row=fila, column=columna, value=titulo)
        columna += 1

    # Datos en la hoja de cálculo
    for usuario in usuarios:
        fila += 1
        columna = 1
        # Obtengo la lista de los campos del modelo
        campos = usuario._meta.get_fields()
        for campo in campos:
            # Recupero el valor del campo para esa instancia del modelo
            ws.cell(row=fila, column=columna, value=Usuario._meta.get_field(campo.name).value_from_object(usuario))
            columna += 1

    wb.save(response)

    return response
    

