from rest_framework import viewsets
import requests
from rest_framework.renderers import JSONRenderer

from app_usuarios.models import Usuario
from app_usuarios.api.serializers import UsuarioSerializer 

class UsuariosViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer


def poblar_usuarios(request):
    response = requests.get('https://randomuser.me/api/?results=20')

    if response.status_code != 200:
        print(F"Error: {response.status_code}")
        quit()

    response_list = response.json().get('results')

    for r in response_list:
        nuevo_usuario = requests.post('http://localhost:8000/api3/usuarios/',
                                    auth=('capresca', 'sieteocho789'),
                                    data={'genero': r.get('gender'),
                                            'titulo': r.get('name').get('title'),
                                            'apellido': r.get('name').get('last'),
                                            'nombre': r.get('name').get('first'),
                                            'ciudad': r.get('location').get('city'),
                                            'pais': r.get('location').get('country'),
                                            'codigo_postal': r.get('location').get('postcode'),
                                            'ciudad': r.get('location').get('city'),
                                            'email': r.get('email'),
                                            'username': r.get('login').get('username'),
                                            'telefono': r.get('cell')})
        print(F'Usuario creado -> {JSONRenderer().render(nuevo_usuario)}')
    
    return "<h1>Terminado</h1>"