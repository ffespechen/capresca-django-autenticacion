from rest_framework.serializers import ModelSerializer 
from app_usuarios.models import Usuario

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id', 'genero', 'titulo', 'apellido', 'nombre', 'ciudad', 'pais', 'codigo_postal', 'email', 'username', 'telefono']
    