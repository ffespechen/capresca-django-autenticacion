from django.urls import path 
from app_usuarios.api.views import poblar_usuarios 

app_name = 'app_usuarios'

urlpatterns = [
    path('', poblar_usuarios, name='poblar_usuarios'),
]