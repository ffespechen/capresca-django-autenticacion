"""
URL configuration for capresca project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.routers import DefaultRouter

from app_dashboard.views import dashboard_view
from app_funciones.views import (
     vista_sin_autenticar, 
     vista_requiere_autenticacion, 
     modelo_listado )
from app_clases.views import ( 
    SinAutenticarView, 
    ConAutenticacionView, 
    ModelosListView )
from app_usuarios.api.views import UsuariosViewSet

# Defino el router para Usuario
router = DefaultRouter()
router.register('usuarios', UsuariosViewSet)



urlpatterns = [
    path('', dashboard_view, name='dashboard'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),


    path('funciones/sin/', vista_sin_autenticar, name='vista_sin_autenticar'),
    path('funciones/con/', vista_requiere_autenticacion, name='vista_con_autenticacion'),
    path('funciones/list/', modelo_listado, name='funcion_listado'),

    path('clases/sin/', SinAutenticarView.as_view(), name='clases_sin'),
    path('clases/con/', ConAutenticacionView.as_view(), name='clases_con'),
    path('clases/list/', ModelosListView.as_view(), name='clases_lista'),

    path('usuarios/', include('app_usuarios.urls', namespace='usuarios')),

    path('api/', include('app_clases.api.urls', namespace='api')),
    path('api2/', include('app_funciones.api.urls', namespace='api2')),

    # urls paths para el router
    path('api3/', include(router.urls)),
    path('poblar/', include('app_usuarios.api.urls')),

    path("admin/", admin.site.urls),


]


urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)