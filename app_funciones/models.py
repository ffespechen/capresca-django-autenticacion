from django.db import models

# Create your models here.
class ModeloPruebaParaFunciones(models.Model):
    marca = models.CharField(max_length=50)
    anio = models.PositiveIntegerField()
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)