from django.shortcuts import render 
from django.contrib.auth.decorators import login_required, permission_required

from .models import ModeloPruebaParaFunciones

# Create your views here.

def vista_sin_autenticar(request):
    return render(request, 'sin_autenticacion.html')


@login_required
def vista_requiere_autenticacion(request):
    return render(request, 'con_autenticacion.html')

@login_required
@permission_required(['app_funciones.view_modelopruebaparafunciones', 
                      'app_funciones.change_modelopruebaparafunciones'], raise_exception=True)
def modelo_listado(request):
    object_list = ModeloPruebaParaFunciones.objects.all()

    return render(request, 'lista.html', { 'object_list': object_list })

