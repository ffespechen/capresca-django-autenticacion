import requests


url = 'https://randomuser.me/api/?results=10'

r = requests.get(url)
resultado = r.json().get('results')
print(type(resultado))

for persona in resultado:
    print(F"{persona.get('name').get('title')}. {persona.get('name').get('last')}, {persona.get('name').get('first')}")


