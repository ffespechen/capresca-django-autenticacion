from django.utils import timezone
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from app_funciones.api.serializers import ModeloFuncionesSerializer
from app_funciones.models import ModeloPruebaParaFunciones


class ModeloFuncionesCreateView(generics.CreateAPIView):
    queryset = ModeloPruebaParaFunciones.objects.all()
    serializer_class = ModeloFuncionesSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated] 

class ModeloFuncionesViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ModeloPruebaParaFunciones.objects.all()
    serializer_class = ModeloFuncionesSerializer

    @action(detail=True, methods=['post', ], authentication_classes=[BasicAuthentication], permission_classes=[IsAuthenticated])
    def actualizar(self, request, *args, **kwargs):
        objeto = self.get_object()
        objeto.modificado = timezone.now()
        objeto.save()
        return Response({'actualizado': F"{objeto.marca} {objeto.anio}",
                         "fecha": objeto.modificado })

