from django.urls import path, include
from rest_framework import routers
from . import views

app_name = 'app_funciones'

router = routers.DefaultRouter()
router.register('app_funciones', views.ModeloFuncionesViewSet)

urlpatterns = [
    path('app_funciones/crear/', views.ModeloFuncionesCreateView.as_view(), name='crear'), 
    path('', include(router.urls)),

]
