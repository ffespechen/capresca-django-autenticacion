from rest_framework import serializers
from app_funciones.models import ModeloPruebaParaFunciones

class ModeloFuncionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModeloPruebaParaFunciones
        fields = ['id', 'marca', 'anio', 'modificado']